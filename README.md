# 包子畫畫練習檔案集中區 #

##圖圖
***

###2019/11/13 [Pixel Art]江江走路三方向
![IMAGE](PixelArt/Jiang/Jiang_walk_up.gif)
![IMAGE](PixelArt/Jiang/Jiang_walk_side.gif)
![IMAGE](PixelArt/Jiang/Jiang_walk_down.gif)
***

###2019/11/11 [Pixel Art]江江走路
![IMAGE](PixelArt/Jiang/Jiang_walk.gif)

***

###2016/06/09 [臨摹]涼宮春日
![IMAGE](臨摹/涼宮春日/涼宮春日.png)

***

###2016/04/10 [臨摹]逢坂大河
![IMAGE](臨摹/逢坂大河/Taiga.png)

***

###2016/02/02 [創作]御坂美琴
![IMAGE](臨摹/御坂美琴/御坂美琴.gif)

***

###2016/01/28 [練習]不同年齡的男生
![IMAGE](繪畫書練習/不同年齡的男生臨摹.png)

***

###2016/01/18 [創作]龍卷
![IMAGE](臨摹/龍卷/龍卷.gif)

***

###2016/01/06 [臨摹]伊莉雅
![IMAGE](臨摹/伊莉雅/伊莉雅.png)

***

###2015/12/06 [臨摹]忍野忍
![IMAGE](臨摹/忍野忍/小忍.png)

***

###2015/11/28 [臨摹]八九寺真宵
![IMAGE](臨摹/八九寺/八九寺真宵.png)

***

###2015/11/21 [臨摹]不知名
![IMAGE](臨摹/不知名/Day1.png)

***